/**
 * MemberController
 *
 * @description :: Server-side logic for managing Members
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport('smtps://scholarleadmail%40gmail.com:scholarlead314@smtp.gmail.com');

module.exports = {
    sendemail: function (req, res) {
        var from = req.param('from');

        var subject = req.param('subject');
        var body = req.param('body');
        var schoolid = req.param('schoolid');
        // var html = req.param('text');
        School.findOne({ id: schoolid }).populate('members').exec(function (err, school) {
            // var to = req.param('to');
            var to = "";// get from db            
            _.forEach(school.members, function (member) {
                if (member.email) {
                    to += member.email+", ";
                }
            });
            to += "scholarleadmail@gmail.com";
            if (to) {
                // setup e-mail data with unicode symbols 
                var mailOptions = {
                    from: from, // sender address 
                    replyTo: from, // sender address 
                    to: to, // list of receivers 
                    subject: subject, // Subject line 
                    text: body, // plaintext body 
                    // html: '<b>Hello world 🐴</b>' // html body 
                };

                // send mail with defined transport object 
                transporter.sendMail(mailOptions, function (error, info) {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message sent: ' + info.response);
                    return res.json('ok');
                });
            }else{
                return res.json('no email sent');
            }
        });

    }
};